//
//  TransactionCell.swift
//  WalletApp
//
//  Created by Arman on 08.01.2024.
//

import UIKit
import SnapKit
import Kingfisher

final class TransactionCell: UICollectionViewCell {
    
    // MARK: - UI
    
    private lazy var cellView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = AppColor.grey90.uiColor
        return view
    }()
    
    private lazy var sourceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var sourceLabel: UILabel = {
        let label = UILabel()
        label.textColor = AppColor.grey0.uiColor
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = AppColor.grey0.uiColor
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var sumLabel: UILabel = {
        let label = UILabel()
        label.textColor = AppColor.grey0.uiColor
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.layer.borderWidth = 1
        label.layer.borderColor = AppColor.purple20.cgColor
        return label
    }()
    
    // MARK: - Initializers
    
    static let identifier = String(describing: TransactionCell.self)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cellView.layer.cornerRadius = 50
        sourceImageView.layer.cornerRadius = 32
        sumLabel.layer.cornerRadius = 32
    }
    
    // MARK: - Configure cell
    
    public func configure(with transaction: Transaction) {
        if let url = URL(string: transaction.getSourceImgLink()) {
            sourceImageView.kf.setImage(with: url)
        }
        sourceLabel.text = transaction.getSource()
        dateLabel.text = transaction.getDate()
        sumLabel.text = transaction.getSum().formatCurrency()
    }
}

extension TransactionCell {
    
    // MARK: - Setup Views
    
    private func setupViews() {
        [sourceImageView,
         sourceLabel,
         dateLabel,
         sumLabel].forEach {
            cellView.addSubview($0)
        }
        contentView.addSubview(cellView)
    }
    
    // MARK: - Setup Constraints
    
    private func setupConstraints() {
        cellView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.height.equalTo(100)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        sourceImageView.snp.makeConstraints { make in
            make.size.equalTo(64)
            make.centerY.equalTo(cellView.snp.centerY)
            make.leading.equalTo(cellView.snp.leading).offset(16)
        }
        
        sourceLabel.snp.makeConstraints { make in
            make.centerY.equalTo(cellView.snp.centerY).offset(-8)
            make.leading.equalTo(sourceImageView.snp.trailing).offset(12)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.centerY.equalTo(cellView.snp.centerY).offset(8)
            make.leading.equalTo(sourceImageView.snp.trailing).offset(12)
        }
        
        sumLabel.snp.makeConstraints { make in
            make.centerY.equalTo(cellView.snp.centerY)
            make.height.equalTo(64)
            make.width.equalTo(120)
            make.trailing.equalTo(cellView.snp.trailing).offset(-16)
        }
    }
}
