//
//  Transaction.swift
//  WalletApp
//
//  Created by Arman on 08.01.2024.
//

import Foundation

// MARK: - Transaction

struct Transaction: Codable {
    
    // MARK: - Fields
    
    private let source: String
    private let sum: Double
    private let date, sourceImgLink: String
    
    // MARK: - Methods
    
    public func getSource() -> String {
        return source
    }
    
    public func getSum() -> Double {
        return sum
    }
    
    public func getDate() -> String {
        return date
    }
    
    public func getSourceImgLink() -> String {
        return sourceImgLink
    }
    
    enum CodingKeys: String, CodingKey {
        case source, sum, date
        case sourceImgLink = "source_img_link"
    }
}

extension Transaction {
    public static let transactions: [Transaction] = [
        Transaction(source: "Amazon",
                    sum: -103.56,
                    date: "May 24, 2022",
                    sourceImgLink: "https://i.ibb.co.com/r0QTJQ6/Amazon-icon.png"),
        Transaction(source: "McDonalds",
                    sum: -34.78,
                    date: "May 12, 2022",
                    sourceImgLink: "https://i.ibb.co.com/YWpMPCL/mcdonalds-icon.webp"),
        Transaction(source: "Apple",
                    sum: -1000.97,
                    date: "May 8, 2022",
                    sourceImgLink: "https://i.ibb.co.com/WyFPZqD/apple-icon.webp"),
        Transaction(source: "Starbucks",
                    sum: -12.50,
                    date: "May 1, 2022",
                    sourceImgLink: "https://i.ibb.co.com/HCjBnbr/starbucks-logo.png")
    ]
}
