//
//  MainViewController.swift
//  WalletApp
//
//  Created by Arman on 08.01.2024.
//

import UIKit
import SnapKit

final class MainViewController: UIViewController {
    
    // MARK: - State
    
    private let transactions = Transaction.transactions
    
    // MARK: - UI
    
    private lazy var transactionCollectionView: UICollectionView = {
        let layout = createLayout()
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: layout)
        collectionView.backgroundColor = AppColor.purple20.uiColor
        collectionView.register(TransactionCell.self,
                                forCellWithReuseIdentifier: TransactionCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
}

extension MainViewController {
    
    // MARK: - Layout
    
    private func createLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { _, _ in
            
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                  heightDimension: .absolute(100))
            
            let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                   heightDimension: .absolute(100))
            
            let layoutGroup = NSCollectionLayoutGroup.vertical(layoutSize: groupSize,
                                                               subitems: [layoutItem])
            
            let transactionsListLayout = NSCollectionLayoutSection(group: layoutGroup)
            transactionsListLayout.interGroupSpacing = 20
            return transactionsListLayout
        }
    }
    
    // MARK: - Setup Views
    
    private func setupViews() {
        view.backgroundColor = AppColor.purple20.uiColor
        view.addSubview(transactionCollectionView)
    }
    
    // MARK: - Setup Contraints
    
    private func setupConstraints() {
        transactionCollectionView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let transactionCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: TransactionCell.identifier,
            for: indexPath) as? TransactionCell else {
            fatalError("Could not cast to InfluencersVideosListCell")
        }
        let transaction = transactions[indexPath.item]
        transactionCell.configure(with: transaction)
        return transactionCell
    }
}
