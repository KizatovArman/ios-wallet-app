//
//  MainTabbarController.swift
//  WalletApp
//
//  Created by Arman on 06.01.2024.
//

import UIKit

final class MainTabbarController: UITabBarController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarAppearance()
        setupTabbar()
    }
    
    // MARK: - Setup Tab Bar
    
    private func setupTabBarAppearance() {
        
    }
    
    private func setupTabbar() {
        let first = UINavigationController(
            rootViewController: MainViewController())
        first.tabBarItem = UITabBarItem(title: nil,
                                        image: AppImage.house.systemImage,
                                        selectedImage: AppImage.houseFill.systemImage)
        
        let second =  UINavigationController(
            rootViewController:  ViewController(color: AppColor.purple100.uiColor))
        second.tabBarItem = UITabBarItem(title: nil,
                                         image: AppImage.creditCard.systemImage,
                                         selectedImage: AppImage.creditCardFill.systemImage)
        
        let third = UINavigationController(
            rootViewController: ViewController(color: AppColor.blue100.uiColor))
        third.tabBarItem = UITabBarItem(title: nil,
                                        image: AppImage.personIcon.systemImage,
                                        selectedImage: AppImage.personIconFill.systemImage)
        
        let forth = UINavigationController(
            rootViewController: ViewController(color: AppColor.grey0.uiColor))
        forth.tabBarItem = UITabBarItem(title: nil,
                                        image: AppImage.chartBar.systemImage,
                                        selectedImage: AppImage.chartBarFill.systemImage)
        
        self.viewControllers = [first, second, third, forth]
    }
}
