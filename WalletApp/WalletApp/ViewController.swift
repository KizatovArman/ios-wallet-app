//
//  ViewController.swift
//  WalletApp
//
//  Created by Arman on 05.01.2024.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Initialize
    
    init(color: UIColor) {
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = color
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
