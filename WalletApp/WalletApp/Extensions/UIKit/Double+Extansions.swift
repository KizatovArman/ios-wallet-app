//
//  Double+Extansions.swift
//  WalletApp
//
//  Created by Arman on 08.01.2024.
//

import Foundation

extension Double {
    func formatCurrency() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "$"
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        
        return numberFormatter.string(from: NSNumber(value: self))!
    }
}
