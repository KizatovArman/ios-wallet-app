//
//  AppImage.swift
//  WalletApp
//
//  Created by Arman on 07.01.2024.
//

import UIKit

protocol AppImageProtocol {
    var rawValue: String { get }
}

extension AppImageProtocol {
    
    var uiImage: UIImage? {
        guard let image = UIImage(named: rawValue) else {
            fatalError("Could not find image with name \(rawValue)")
        }
        return image
    }
    
    var systemImage: UIImage? {
        guard let image = UIImage(systemName: rawValue) else {
            fatalError("Could not find image with name \(rawValue)")
        }
        return image
    }
}

enum AppImage: String, AppImageProtocol {
    
    // MARK: - Tabbar
    
    case house = "house"
    case houseFill = "house.fill"
    case creditCard = "creditcard"
    case creditCardFill = "creditcard.fill"
    case personIcon = "person"
    case personIconFill = "person.fill"
    case chartBar = "chart.bar"
    case chartBarFill = "chart.bar.fill"
}
