//
//  AppColor.swift
//  WalletApp
//
//  Created by Arman on 06.01.2024.
//

import UIKit

protocol AppColorProtocol {
    var rawValue: String { get }
}

extension AppColorProtocol {
    var uiColor: UIColor {
        guard let color = UIColor.init(named: rawValue) else {
            fatalError("Could not find color with name \(rawValue)")
        }
        return color
    }
    
    var cgColor: CGColor {
        return uiColor.cgColor
    }
}

enum AppColor: String, AppColorProtocol {
    case blue40
    case blue80
    case blue100
    case green100
    case grey0
    case grey70
    case grey90
    case purple20
    case purple100
}
